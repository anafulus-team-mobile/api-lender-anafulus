<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Models\DetailLenders;

class ExampleController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function register(Request $request){
        try {
            $dLender = new DetailLenders();
            $dLender->id_lender = $request->id_lender;
            $dLender->full_name = $request->full_name;
            $dLender->pin = $request->pin;
            $dLender->email = $request->email;
            $dLender->phone_number = $request->phone_number;
            $dLender->saveOrFail();

            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Berhasil mendaftar',
            ];    
        } catch (Exception $e) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal mendaftar',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function lender(Request $request){
        
    }

    //
}
